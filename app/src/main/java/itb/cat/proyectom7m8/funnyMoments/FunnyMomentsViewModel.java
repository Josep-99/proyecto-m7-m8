package itb.cat.proyectom7m8.funnyMoments;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class FunnyMomentsViewModel extends ViewModel {

    private MutableLiveData<String> eText;

    public FunnyMomentsViewModel(){
        eText = new MutableLiveData<>();
        eText.setValue("Momentos Divertidos");
    }
    public LiveData<String> getText(){
      return eText;
    }
}
