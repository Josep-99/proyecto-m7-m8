package itb.cat.proyectom7m8.funnyMoments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.proyectom7m8.R;
import itb.cat.proyectom7m8.funnyMoments.Service.ServiceVideo;


public class FunnyMomentsFragment extends Fragment {

    Button botonVideo1;
    Button botonVideo2;

    private VideoView video1;
    private VideoView video2;


    private FunnyMomentsViewModel funnyMomentsViewModel;
    public static FunnyMomentsFragment newInstance() {
        return new FunnyMomentsFragment();
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View equipoView = inflater.inflate(R.layout.funnymoments_fragment, container, false);

        video1 = equipoView.findViewById(R.id.video1);
        video2 = equipoView.findViewById(R.id.video2);


        return equipoView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        funnyMomentsViewModel = ViewModelProviders.of(this).get(FunnyMomentsViewModel.class);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        String url = "https://thumbs.gfycat.com/QuestionableBareButterfly-mobile.mp4";
        video1.setVideoURI(Uri.parse(url));
        video1.requestFocus();

        String url2 = "https://thumbs.gfycat.com/DecentFakeFireant-mobile.mp4";
        video2.setVideoURI(Uri.parse(url2));
        video2.requestFocus();


    }

    @OnClick(R.id.buttonVideo1)
    public void onClickeFirstVideo(){
        getActivity().startService(new Intent(getContext(), ServiceVideo.class));
        if(video1.isPlaying()) {
            video1.stopPlayback();
        }else if(!video1.isPlaying()){
            video1.resume();
        }
        video1.start();
    }

    @OnClick(R.id.buttonVideo2)
    public void onClickeSecondVideo(){
        getActivity().startService(new Intent(getContext(), ServiceVideo.class));
        if(video2.isPlaying()) {
            video2.stopPlayback();
        }else if(!video2.isPlaying()){
            video2.resume();
        }
        video2.start();
    }


}
