package itb.cat.proyectom7m8.contadorPalos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.proyectom7m8.R;

public class ContadorFragment extends Fragment {
    TextView textContador;
    Button plusButton;
    Button lessButton;
    Button sendContador;

    private ContadorViewModel contadorViewModel;

    public static ContadorFragment newInstance() {
        return new ContadorFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View contadorView = inflater.inflate(R.layout.contador_tiros_fragment, container, false);

        textContador = contadorView.findViewById(R.id.text_contador);
        lessButton = contadorView.findViewById(R.id.lessButton);
        plusButton = contadorView.findViewById(R.id.plusButton);
        sendContador = contadorView.findViewById(R.id.continueButton);

        return contadorView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        contadorViewModel = ViewModelProviders.of(this).get(ContadorViewModel.class);

        contadorViewModel.getCantidadPalos().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer t) {
                textContador.setText(String.valueOf(t));
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

    }

    @OnClick(R.id.lessButton)
    public void setLessButton(){
        contadorViewModel.restarPalo();
    }

    @OnClick(R.id.plusButton)
    public void setPlusButton(){
        contadorViewModel.sumarPalo();
    }
    @OnClick(R.id.continueButton)
    public void setEnviar (){
        contadorViewModel.setContextFinal(contadorViewModel.getCantidadPalos().getValue());
        Navigation.findNavController(getView()).navigate(R.id.action_nav_contador_palos_to_nav_estadisticas);
    }
}
