package itb.cat.proyectom7m8.contadorPalos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ContadorViewModel extends ViewModel {

    private MutableLiveData<Integer> conText;
    private MutableLiveData<Integer> conTextFinal;
    private int cantidadPalos;

    public ContadorViewModel(){
        conText = new MutableLiveData<>();
        conTextFinal = new MutableLiveData<>();
        conText.setValue(0);
        conTextFinal.setValue(0);
    }

    public LiveData<Integer> getCantidadPalos(){
      return conText;
    }

    public LiveData<Integer> getTextFinal(){
        return conTextFinal;
    }

    public LiveData<Integer> setContextFinal(int i){
        conTextFinal.setValue(i);
        return conTextFinal;
    }

    public LiveData<Integer> sumarPalo(){
        cantidadPalos = cantidadPalos + 1;
        conText.setValue(cantidadPalos);
        return conText;
    }

    public LiveData<Integer> restarPalo(){
        cantidadPalos = cantidadPalos - 1;
        conText.setValue(cantidadPalos);
        return conText;
    }
}
