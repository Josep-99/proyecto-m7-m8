package itb.cat.proyectom7m8.calendar;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CalendarViewModel extends ViewModel {

    private MutableLiveData<String> cText;

    public CalendarViewModel(){
        cText = new MutableLiveData<>();
        cText.setValue("Fragmento Calendario(implementar Calendario)");
    }
    public LiveData<String> getText(){
      return cText;
    }
}
