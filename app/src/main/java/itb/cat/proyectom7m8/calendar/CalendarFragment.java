package itb.cat.proyectom7m8.calendar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import itb.cat.proyectom7m8.R;

public class CalendarFragment extends Fragment {
    private CalendarViewModel calendarViewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View calendarView = inflater.inflate(R.layout.calendario_fragment, container, false);




        return calendarView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        calendarViewModel = ViewModelProviders.of(this).get(CalendarViewModel.class);
    }

}
