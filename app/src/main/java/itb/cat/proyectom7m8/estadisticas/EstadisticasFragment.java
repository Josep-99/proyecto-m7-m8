package itb.cat.proyectom7m8.estadisticas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;
import itb.cat.proyectom7m8.R;
import itb.cat.proyectom7m8.contadorPalos.ContadorViewModel;

public class EstadisticasFragment extends Fragment {

    private ContadorViewModel contadorViewModel;
    private RecyclerView recyclerView;

    TextView data;
    TextView tirosPuerta;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View estadisticasView = inflater.inflate(R.layout.estadisticas_fragment, container, false);
        data = estadisticasView.findViewById(R.id.fecha);
        tirosPuerta = estadisticasView.findViewById(R.id.palos);


        return estadisticasView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        contadorViewModel = ViewModelProviders.of(this).get(ContadorViewModel.class);
/*
        contadorViewModel.getCantidadPalos().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer t) {
                tirosPuerta.setText(String.valueOf(t));
            }
        });


 */
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
    }


}
