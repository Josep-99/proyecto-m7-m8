package itb.cat.proyectom7m8.registroRango;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class RegistroRangoViewModel extends ViewModel {

    private MutableLiveData<String> rText;

    public RegistroRangoViewModel(){
        rText = new MutableLiveData<>();
        rText.setValue("Selecciona la fecha: ");
    }
    public LiveData<String> getText(){
      return rText;
    }
}
