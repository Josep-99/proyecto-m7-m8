package itb.cat.proyectom7m8.registroRango;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.google.android.material.snackbar.Snackbar;

import butterknife.ButterKnife;
import butterknife.OnClick;
import itb.cat.proyectom7m8.R;

public class RegistroRangoFragment extends Fragment {
    TextView textRegistro;
    CheckBox Bronce3;
    CheckBox Bronce2;
    CheckBox Bronce1;
    CheckBox Plata3;
    CheckBox Plata2;
    CheckBox Plata1;
    CheckBox Oro3;
    CheckBox Oro2;
    CheckBox Oro1;
    CheckBox Elite3;
    CheckBox Elite2;
    CheckBox Elite1;
    Button next;


    private RegistroRangoViewModel registroRangoViewModel;

    public static RegistroRangoFragment newInstance() {
        return new RegistroRangoFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View registroView = inflater.inflate(R.layout.registrar_rango_alcanzado_fragment, container, false);
        textRegistro = registroView.findViewById(R.id.text_registrar_rango);
        Bronce3 = registroView.findViewById(R.id.bronce3);
        Bronce2 = registroView.findViewById(R.id.bronce2);
        Bronce1 = registroView.findViewById(R.id.bronce1);
        Plata3 = registroView.findViewById(R.id.plata3);
        Plata2 = registroView.findViewById(R.id.plata2);
        Plata1 = registroView.findViewById(R.id.plata1);
        Oro3 = registroView.findViewById(R.id.oro3);
        Oro2 = registroView.findViewById(R.id.oro2);
        Oro1 = registroView.findViewById(R.id.oro1);
        Elite3 = registroView.findViewById(R.id.elite3);
        Elite2 = registroView.findViewById(R.id.elite2);
        Elite1 = registroView.findViewById(R.id.elite1);
        next = registroView.findViewById(R.id.buttonEnviarRango);
        return registroView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        registroRangoViewModel = ViewModelProviders.of(this).get(RegistroRangoViewModel.class);

        registroRangoViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String t) {
                textRegistro.setText(t);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }


    @OnClick(R.id.buttonEnviarRango)
    public void nextFragment(){
        int limite= 0;
                if(Bronce3.isChecked()) {
                    limite = limite + 1;
                }else if(Bronce2.isChecked()){
                    limite = limite +1;
                }else if(Bronce1.isChecked()){
                    limite = limite +1;
                }else if(Plata3.isChecked()){
                    limite = limite +1;
                }else if(Plata2.isChecked()){
                    limite = limite +1;
                }else if(Plata1.isChecked()){
                    limite = limite +1;
                }else if(Oro3.isChecked()){
                    limite = limite +1;
                }else if(Oro2.isChecked()){
                    limite = limite +1;
                }else if(Oro1.isChecked()){
                    limite = limite +1;
                }else if(Elite3.isChecked()){
                    limite = limite +1;
                }else if(Elite2.isChecked()){
                    limite = limite +1;
                }else if(Elite1.isChecked()) {
                    limite = limite + 1;
                }

        if(limite==1){
            Navigation.findNavController(getView()).navigate(R.id.action_nav_registro_rango_to_nav_estadisticas);
        }else{
            Snackbar.make(getView(), "Por favor, Selecciona solo una opcion", Snackbar.LENGTH_LONG).show();
        }
    }
/*
    public void onCheckboxClicked(View view){
        int limite = 0;
        boolean checked = ((CheckBox ) view).isChecked();

        switch (view.getId()){
            case R.id.bronce3:
                if(checked)
                    limite = limite+1;
                    else
                        break;

            case R.id.bronce2:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.bronce1:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.plata3:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.plata2:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.plata1:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.oro3:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.oro2:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.oro1:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.elite3:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.elite2:
                if(checked)
                    limite = limite+1;
                else
                    break;
            case R.id.elite1:
                if(checked)
                    limite = limite+1;
                else
                    break;
        }
        if(limite==1){
            Navigation.findNavController(getView()).navigate(R.id.action_nav_registro_rango_to_nav_estadisticas);
        }else{
            Snackbar.make(view, "Por favor, Selecciona solo una opcion", Snackbar.LENGTH_LONG).show();
        }
    }
*/

}
